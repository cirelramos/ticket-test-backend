<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('oauth/login', 'Auth\Controllers\LoginController@login')->name('login');
Route::get('oauth/user', 'User\Controllers\UserController@user')->name('users.session');
Route::get('oauth/logout', 'Auth\Controllers\LoginController@logout')->name('logout');
Route::resource('users','User\Controllers\UserController');
Route::post( 'groups/{group}/associate/user/', 'Groups\Controllers\GroupController@associateUser' )->name( 'groups.associate_user' );
Route::resource('groups','Groups\Controllers\GroupController');
Route::resource('notes','Notes\Controllers\NoteController');
Route::resource('tickets','Tickets\Controllers\TicketController');
