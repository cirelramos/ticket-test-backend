@extends('Notification.Resource.layout.app')

@section('content')

    <p>
        En tu grupo han creado una nueva nota en el sistema
    </p>


    <table>
        <tr>
            <td>
                <p>
                    <b>NOTA:</b> {{$note->name}}
                </p>
                <p>
                    <b>DESCRIPCIÓN:</b> {{$note->description}}
                </p>
            </td>
            <td>
                @foreach($note->images as $image)
                    <img width="250" style="margin-top: 25px" src="{{$image->path}}" alt="">
                @endforeach
            </td>
        </tr>
    </table>

@endsection
