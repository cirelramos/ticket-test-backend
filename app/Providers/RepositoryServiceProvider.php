<?php

namespace App\Providers;

use App\Core\Base\Repositories\Interfaces\BaseRepositoryInterface;
use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\Groups\Repositories\Interfaces\GroupRepositoryInterface;
use App\Core\Groups\Repositories\Usages\GroupRepository;
use App\Core\Images\Repositories\interfaces\ImageRepositoryInterface;
use App\Core\Images\Repositories\usages\ImageRepository;
use App\Core\Notes\Repositories\Interfaces\NoteRepositoryInterface;
use App\Core\Notes\Repositories\Usages\NoteRepository;
use App\Core\Tickets\Repositories\Interfaces\TicketRepositoryInterface;
use App\Core\Tickets\Repositories\Usages\TicketRepository;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;
use App\Core\User\Repositories\Usages\UserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(
            BaseRepositoryInterface::class,
            BaseRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );

        $this->app->bind(
            GroupRepositoryInterface::class,
            GroupRepository::class
        );

        $this->app->bind(
            NoteRepositoryInterface::class,
            NoteRepository::class
        );


        $this->app->bind(
            ImageRepositoryInterface::class,
            ImageRepository::class
        );

        $this->app->bind(
            TicketRepositoryInterface::class,
            TicketRepository::class
        );

    }

}
