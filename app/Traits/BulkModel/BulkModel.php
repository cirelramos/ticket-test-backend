<?php

namespace App\Traits\BulkModel;

use Illuminate\Support\Facades\DB;

/**
 * Trait BulkModel
 *
 * @package App\Traits\BulkModel
 */
trait BulkModel
{
    /**
     * @param $table
     * @param $values
     * @param $index
     * @return bool
     */
    public function updateBatch($table, $values, $index): bool
    {
        $table = $table->getTable();
        $final = [];
        $ids = [];

        if (!count($values)) {
            return false;
        }

        if (!isset($index) && empty($index)) {
            return false;
        }

        foreach ($values as $val) {
            $ids[] = $val[$index];
            foreach (array_keys($val) as $field) {
                if ($field !== $index) {
                    $value = ($val[$field] === null ? 'NULL' : '"' . $this->mysqlEscape($val[$field]) . '"');
                    $final[$field][] = 'WHEN `' . $index . '` = "' . $val[$index] . '" THEN ' . $value . ' ';
                }
            }
        }

        $cases = '';
        foreach ($final as $k => $v) {
            $cases .= '`' . $k . '` = (CASE ' . implode("\n", $v) . "\n" . 'ELSE `' . $k . '` END), ';
        }

        $query = "UPDATE `$table` SET " . substr($cases, 0, -2) . " WHERE `$index` IN(" . '"' . implode('","', $ids) . '"' . ');';

        return DB::statement($query);
    }

    /**
     * @param $inp
     * @return array|mixed
     */
    protected function mysqlEscape($inp)
    {
        if (is_array($inp)) {
            return array_map(__METHOD__, $inp);
        }
        if (!empty($inp) && is_string($inp)) {
            return str_replace(
                ['\\', "\0", "\n", "\r", "'", '"', "\x1a"],
                ['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'],
                $inp);
        }
        return $inp;
    }
}