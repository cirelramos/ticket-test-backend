<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Trait ApiResponse
 * @package App\Traits
 */
trait ApiResponse
{

    /**
     * @param array  $data
     * @param string $message
     * @param int    $code
     * @return JsonResponse
     */
    public function errorResponse($data = [], $message = '', $code = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {

        return response()->json(['code' => $code, 'message' => $message, 'data' => $data], $code);

    }

    /**
     * @param array  $data
     * @param string $message
     * @param int    $code
     * @return JsonResponse
     */
    public function successRespons($data = [], $message = '', $code = Response::HTTP_OK): JsonResponse
    {

        return response()->json(['code' => $code, 'message' => $message, 'data' => $data,], $code);

    }
}
