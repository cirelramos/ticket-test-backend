<?php

namespace App\Core\User\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class TypesUser
 *
 * @package App\Core\User\Models
 * @property int $id_type_user
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser whereIdTypeUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\TypeUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TypeUser extends Model
{

    /**
     * Database table name
     */
    protected $table = 'types_users';

    /**
     * Mass assignable columns
     */
    protected $fillable = [ 'name' ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * users
     *
     * @return HasMany
     */
    public function users()
    {

        return $this->hasMany( User::class, 'id_type_user' );
    }

}
