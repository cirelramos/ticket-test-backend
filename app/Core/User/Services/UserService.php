<?php

namespace App\Core\User\Services;

use App\Core\User\Models\User;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;
use App\Core\User\Requests\StoreUserRequest;
use App\Core\User\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class UserService
 * @package App\Core\User\Services
 */
class UserService
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct( UserRepositoryInterface $userRepository )
    {

        $this->userRepository = $userRepository;
    }

    public function list( $request )
    {

        $columns = [ 'id', 'users.name as name', 'email', 'users.id_type_user', 'ut.name as type_user_name' ];

        return $this->userRepository->listAllUsers()->get( $columns );

    }

    /**
     * @param Request $request
     * @return array
     */
    public function create( Request $request ): array
    {

        $typeUser = $this->userRepository->getAllTypeUsers()->get( [ 'id_type_user', 'name' ] );

        return ['type_user' => $typeUser];

    }

    /**
     * @param User $user
     * @return mixed
     */
    public function show( User $user )
    {

        $columns = [ 'id', 'users.name as name', 'email', 'users.id_type_user', 'ut.name as type_user_name' ];

        return $this->userRepository->show( $user )->first( $columns );

    }

    /**
     * @param $storeUserRequest
     * @return User
     */
    public function save( StoreUserRequest $storeUserRequest ): User
    {

        $user                = $this->userRepository->getModel();
        $input               = $storeUserRequest->validated();
        $input[ 'password' ] = bcrypt( $input[ 'password' ] );
        $user->fill( $input );

        $user = $this->userRepository->save( $user );

        $user->token = $user->createToken( 'Personal Access Token' );

        return $user;

    }

    /**
     * @param UpdateUserRequest $updateUserRequest
     * @param User              $user
     * @return User
     */
    public function update( UpdateUserRequest $updateUserRequest, User $user ): User
    {

        $user->fill( $updateUserRequest->validated() );

        return $this->userRepository->save( $user );

    }

    /**
     * @param User $user
     * @return User
     */
    public function destroy( User $user ): User
    {

        return $this->userRepository->destroy( $user );

    }

}
