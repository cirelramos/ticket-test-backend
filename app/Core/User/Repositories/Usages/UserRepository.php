<?php

namespace App\Core\User\Repositories\Usages;

use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\User\Models\TypeUser;
use App\Core\User\Models\User;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;
use Exception;
use Illuminate\Database\Query\Builder;

/**
 * Class UserRepository
 * @package App\Core\User\Repositories\Usages
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * @var User
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param User $model
     */
    public function __construct( User $model )
    {

        $this->model = $model;
        parent::__construct( $model );
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function getAllUserByIdsQuery( array $ids )
    {

        return $this->getModel()->whereIn( 'id', $ids );

    }

    /**
     * @return User
     */
    public function getModel(): User
    {

        return $this->model;
    }

    /**
     * @return TypeUser
     */
    public function getModelTypeUser(): TypeUser
    {

        return new TypeUser();

    }

    /**
     * @return Builder
     */
    public function getAllTypeUsers()
    {

        return $this->getModelTypeUser()->orderBy( 'name', 'asc');
    }

    /**
     * @return Builder
     */
    public function listAllUsers()
    {

        return $this->getModel()
            ->join( 'types_users as ut', 'users.id_type_user', '=', 'ut.id_type_user' )
            ->orderBy( 'email', 'asc' )
            ;

    }

    /**
     * @param User $user
     * @return User
     */
    public function save( User $user ): User
    {

        $user->save();

        return $user;

    }

    /**
     * @param User $user
     * @return User
     * @throws Exception
     */
    public function destroy( User $user ): User
    {

        $user->delete();

        return $user;

    }

    public function show( User $user )
    {

        return $this->getModel()
            ->join( 'types_users as ut', 'users.id_type_user', '=', 'ut.id_type_user' )
            ->where( 'id', $user->id )
            ;

    }

}
