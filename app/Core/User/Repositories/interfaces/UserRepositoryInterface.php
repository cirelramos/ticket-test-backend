<?php

namespace App\Core\User\Repositories\interfaces;

use App\Core\User\Models\TypeUser;
use App\Core\User\Models\User;
use Illuminate\Database\Query\Builder;

/**
 * Class UserRepositoryInterface
 * @package App\Core\User\Repositories\interfaces
 */
interface UserRepositoryInterface
{

    /**
     * @param array $ids
     * @return mixed
     */
    public function getAllUserByIdsQuery( array $ids );
    /**
     * @return User
     */
    public function getModel(): User;

    /**
     * @return TypeUser
     */
    public function getModelTypeUser(): TypeUser;

    /**
     * @return Builder
     */
    public function getAllTypeUsers();

    /**
     * @return mixed
     */
    public function listAllUsers();

    /**
     * @param User $user
     * @return User
     */
    public function save( User $user ): User;

    /**
     * @param User $user
     * @return User
     */
    public function destroy( User $user ): User;

    public function show( User $user );

}
