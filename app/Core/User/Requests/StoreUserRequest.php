<?php

namespace App\Core\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreUserRequest
 * @package App\Core\User\Request
 */
class StoreUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {

        return [
            'name'         => 'required|max:250',
            'email'        => 'required|unique:users|max:250',
            'id_type_user' => 'required|numeric|exists:types_users,id_type_user',
            'password'     => 'required|max:250',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [

        ];
    }
}

