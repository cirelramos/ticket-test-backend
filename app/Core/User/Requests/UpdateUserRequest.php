<?php

namespace App\Core\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreUserRequest
 * @package App\Core\User\Request
 */
class UpdateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {

        return [
            'name'         => 'nullable|max:250',
            'email'        => 'nullable|unique:users|max:250',
            'id_type_user' => 'nullable|numeric|exists:types_users,id_type_user',
            'password'     => 'nullable|max:250',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [

        ];
    }
}

