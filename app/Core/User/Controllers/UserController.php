<?php

namespace App\Core\User\Controllers;

use App\Core\User\Models\User;
use App\Core\User\Requests\StoreUserRequest;
use App\Core\User\Requests\UpdateUserRequest;
use App\Core\User\Services\UserService;
use App\Http\Controllers\ApiController;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class UserController
 * @package App\Core\User\Controllers
 */
class UserController extends ApiController
{

    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct( UserService $userService )
    {

        $this->middleware( 'auth:api' )->except( [ 'store' ] );
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index( Request $request ): JsonResponse
    {

        $users = $this->userService->list( $request );

        return $this->successRespons($users, '', Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create( Request $request ): JsonResponse
    {

        $response = $this->userService->create( $request );

        return $this->successRespons($response, '', Response::HTTP_ACCEPTED);

    }

    public function user( Request $request )
    {

        $user = Auth::user();
        $user = $this->userService->show( $user );

        return $this->successRespons($user, '', Response::HTTP_ACCEPTED);

    }

    /**
     * @param StoreUserRequest $storeUserRequest
     * @return JsonResponse|null
     * @throws Exception
     */
    public function store( StoreUserRequest $storeUserRequest ): ?JsonResponse
    {

        $errorMessage   = 'error al crear el usuario';
        $successMessage = 'usuario creado exitosamente';

        try {

            DB::beginTransaction();

            $user = $this->userService->save( $storeUserRequest );
            DB::commit();

            return $this->successRespons($user, $successMessage, Response::HTTP_ACCEPTED);
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500);
        }

    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function show( User $user ): JsonResponse
    {

        $user = $this->userService->show( $user );

        return $this->successRespons($user, '', Response::HTTP_ACCEPTED);

    }

    /**
     * @param UpdateUserRequest $updateUserRequest
     * @param User              $user
     * @return JsonResponse|null
     * @throws Exception
     */
    public function update( UpdateUserRequest $updateUserRequest, User $user ): ?JsonResponse
    {

        $errorMessage   = 'error al actualizar el usuario';
        $successMessage = 'usuario actualizado exitosamente';

        try {

            DB::beginTransaction();

            $user = $this->userService->update( $updateUserRequest, $user );
            DB::commit();

            return $this->successRespons($user, $successMessage, Response::HTTP_ACCEPTED);
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500);
        }
    }

    /**
     * @param User $user
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy( User $user ): ?JsonResponse
    {

        $errorMessage   = 'error al eliminar el usuario';
        $successMessage = 'usuario eliminado exitosamente';

        try {

            DB::beginTransaction();

            $user = $this->userService->destroy( $user );
            DB::commit();

            return $this->successRespons($user, $successMessage, Response::HTTP_ACCEPTED);
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500);
        }
    }

}
