<?php

namespace App\Core\Tickets\Repositories\Interfaces;

use App\Core\Tickets\Models\Ticket;

/**
 * Interface TicketRepositoryInterface
 * @package App\Core\Tickets\Repositories\Interfaces
 */
interface TicketRepositoryInterface
{

    /**
     * @return Ticket
     */
    public function getModel(): Ticket;

    /**
     * @param Ticket $ticket
     * @return mixed
     */
    public function save( Ticket $ticket );

    /**
     * @param Ticket $ticket
     * @return mixed
     */
    public function updateTicket( Ticket $ticket );

    /**
     * @return mixed
     */
    public function listTickets();

    /**
     * @param Ticket $ticket
     * @return Ticket
     */
    public function destroy( Ticket $ticket ): Ticket;

    /**
     * @param Ticket $ticket
     * @return mixed
     */
    public function show( Ticket $ticket );
}
