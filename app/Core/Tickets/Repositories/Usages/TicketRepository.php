<?php

namespace App\Core\Tickets\Repositories\Usages;

use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\Tickets\Models\Ticket;
use App\Core\Tickets\Repositories\Interfaces\TicketRepositoryInterface;

/**
 * Class TicketRepository
 * @package App\Core\Tickets\Repositories\Usages
 */
class TicketRepository extends BaseRepository implements TicketRepositoryInterface
{

    /**
     * @var Ticket
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Ticket $model
     */
    public function __construct( Ticket $model )
    {

        $this->model = $model;
        parent::__construct( $model );
    }

    /**
     * @param Ticket $ticket
     * @return Ticket|bool
     */
    public function save( Ticket $ticket )
    {

        $ticket->save();

        return $ticket;

    }

    /**
     * @param Ticket $ticket
     * @return Ticket
     */
    public function updateTicket( Ticket $ticket )
    {

        $ticket->save();

        return $ticket;

    }

    /**
     * @return mixed
     */
    public function listTickets()
    {

        return $this->getModel()
            ->join( 'users as u', 'tickets.id_user', '=', 'u.id' )
            ;

    }

    /**
     * @return Ticket
     */
    public function getModel(): Ticket
    {

        return $this->model;
    }

    public function destroy( Ticket $ticket ): Ticket
    {

        $ticket->delete();

        return $ticket;

    }

    public function show( Ticket $ticket )
    {

        return $this->getModel()
            ->where( 'id_ticket', $ticket->id_ticket )
            ->join( 'users as u', 'tickets.id_user', '=', 'u.id' )
            ;

    }
}
