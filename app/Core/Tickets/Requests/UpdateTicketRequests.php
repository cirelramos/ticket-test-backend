<?php

namespace App\Core\Tickets\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreNoteRequests
 * @package App\Core\Notes\Requests
 */
class UpdateTicketRequests extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {

        return [
            'id_user'        => 'sometimes|required|numeric|exists:users,id',
            'ticket_requested' => 'sometimes|required|numeric|min:0|max:1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [

        ];
    }
}
