<?php

namespace App\Core\Tickets\Services;

use App\Core\Tickets\Models\Ticket;
use App\Core\Tickets\Repositories\Interfaces\TicketRepositoryInterface;
use App\Core\Tickets\Requests\StoreTicketRequests;
use App\Core\Tickets\Requests\UpdateTicketRequests;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class TicketService
 * @package App\Core\Tickets\Services
 */
class TicketService
{

    /**
     * @var TicketRepositoryInterface
     */
    private $ticketRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * TicketService constructor.
     * @param TicketRepositoryInterface $ticketRepository
     * @param UserRepositoryInterface   $userRepository
     */
    public function __construct( TicketRepositoryInterface $ticketRepository, UserRepositoryInterface $userRepository )
    {

        $this->ticketRepository = $ticketRepository;
        $this->userRepository   = $userRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index( Request $request )
    {

        $columns = [ 'tickets.id_ticket', 'u.id as id_user', 'u.name as user_name', ];

        return $this->ticketRepository->listTickets()->get( $columns );

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create( Request $request )
    {

        $columns = [ 'id', 'users.name as name', 'email', 'users.id_type_user', 'ut.name as type_user_name' ];
        $users   = $this->userRepository->listAllUsers()->get( $columns );

        return [ 'users' => $users ];
    }

    /**
     * @param $storeTicketRequests
     * @return mixed
     */
    public function save( StoreTicketRequests $storeTicketRequests )
    {

        $ticket = $this->ticketRepository->getModel();
        $ticket->fill( $storeTicketRequests->validated() );

        return $this->ticketRepository->save( $ticket );

    }

    /**
     * @param        $updateTicketRequests
     * @param Ticket $ticket
     * @return mixed
     */
    public function update( UpdateTicketRequests $updateTicketRequests, Ticket $ticket )
    {

        $ticket->fill( $updateTicketRequests->validated() );

        return $this->ticketRepository->updateTicket( $ticket );

    }

    /**
     * @param Ticket $ticket
     * @return mixed
     */
    public function show( Ticket $ticket )
    {

        $columns = [ 'tickets.id_ticket', 'u.id as id_user', 'u.name as user_name', ];

        return $this->ticketRepository->show( $ticket )->get( $columns );
    }

    /**
     * @param Ticket $ticket
     * @return Ticket
     */
    public function destroy( Ticket $ticket )
    {

        return $this->ticketRepository->destroy( $ticket );

    }
}
