<?php

namespace App\Core\Tickets\Controllers;

use App\Core\Tickets\Models\Ticket;
use App\Core\Tickets\Requests\StoreTicketRequests;
use App\Core\Tickets\Requests\UpdateTicketRequests;
use App\Core\Tickets\Services\TicketService;
use App\Http\Controllers\ApiController;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketController
 * @package App\Core\Tickets\Controllers
 */
class TicketController extends ApiController
{

    /**
     * @var TicketService
     */
    private $ticketService;

    /**
     * TicketController constructor.
     * @param TicketService $ticketService
     */
    public function __construct( TicketService $ticketService )
    {

        $this->middleware( 'auth:api' );
        $this->ticketService = $ticketService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index( Request $request ): JsonResponse
    {

        $tickets = $this->ticketService->index($request);

        return $this->successRespons($tickets, '', Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create( Request $request  ): JsonResponse
    {
        $response = $this->ticketService->create( $request);

        return $this->successRespons($response, '', Response::HTTP_ACCEPTED);
    }

    /**
     * @param StoreTicketRequests $storeTicketRequests
     * @return JsonResponse|null
     * @throws Exception
     */
    public function store( StoreTicketRequests $storeTicketRequests ): ?JsonResponse
    {

        $errorMessage   = 'error al crear el ticket';
        $successMessage = 'ticket creada exitosamente';

        try {

            DB::beginTransaction();

            $ticket = $this->ticketService->save( $storeTicketRequests );
            DB::commit();

            return $this->successRespons($ticket, $successMessage, Response::HTTP_CREATED);
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500);
        }

    }

    /**
     * @param Ticket $ticket
     * @return JsonResponse
     */
    public function show( Ticket $ticket ): JsonResponse
    {

        $ticket = $this->ticketService->show( $ticket );

        return $this->successRespons($ticket, '', Response::HTTP_ACCEPTED);

    }

    /**
     * @param UpdateTicketRequests $updateTicketRequests
     * @param Ticket               $ticket
     * @return JsonResponse|null
     * @throws Exception
     */
    public function update( UpdateTicketRequests $updateTicketRequests, Ticket $ticket ): ?JsonResponse
    {

        $errorMessage   = 'error al actualizar el ticket';
        $successMessage = 'ticket actualizado exitosamente';

        try {

            DB::beginTransaction();

            $ticket = $this->ticketService->update( $updateTicketRequests, $ticket );
            DB::commit();

            return $this->successRespons($ticket, $successMessage, Response::HTTP_CREATED);
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500);
        }
    }

    /**
     * @param Ticket $ticket
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy( Ticket $ticket ): ?JsonResponse
    {

        $errorMessage   = 'error al eliminar el ticket';
        $successMessage = 'ticket eliminado exitosamente';

        try {

            DB::beginTransaction();

            $ticket = $this->ticketService->destroy( $ticket );
            DB::commit();

            return $this->successRespons($ticket, $successMessage, Response::HTTP_CREATED);
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500);
        }
    }



}
