<?php

namespace App\Core\Tickets\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Ticket
 * @package App\Core\Tickets\Models
 */
class Ticket extends Model
{

    /**
     * Database table name
     */
    protected $table = 'tickets';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_ticket';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'ticket_requested',
        'id_user',
    ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * idUser
     *
     * @return BelongsTo
     */
    public function idUser()
    {

        return $this->belongsTo( User::class, 'id_user' );
    }

}
