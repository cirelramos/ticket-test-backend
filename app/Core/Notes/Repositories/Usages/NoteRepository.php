<?php

namespace App\Core\Notes\Repositories\Usages;

use App\Core\Base\Repositories\Usages\BaseRepository;
use App\Core\Notes\Models\Note;
use App\Core\Notes\Models\NotesHasImage;
use App\Core\Notes\Repositories\Interfaces\NoteRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NoteRepository
 * @package App\Core\Notes\Repositories\Usages
 */
class NoteRepository extends BaseRepository implements NoteRepositoryInterface
{

    /**
     * @var Note
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Note $model
     */
    public function __construct( Note $model )
    {

        $this->model = $model;
        parent::__construct( $model );
    }

    /**
     * @param Note $note
     * @return Note|bool
     */
    public function save( Note $note )
    {

        $note->save();

        return $note;

    }

    /**
     * @param Note $note
     * @return Note
     */
    public function updateNote( Note $note )
    {

        $note->save();

        return $note;

    }

    /**
     * @param       $idImage
     * @param Note  $note
     * @return mixed
     */
    public function deleteUserAssociateToNote( $idImage, Note $note )
    {

        return $this->getNotesHasImages()::where( 'id_image', $idImage )->where( 'id_note', $note->id_note )->delete();

    }

    /**
     * @return NotesHasImage
     */
    public function getNotesHasImages(): NotesHasImage
    {

        return new NotesHasImage();

    }

    /**
     * @param Note  $note
     * @param       $idImage
     * @return NotesHasImage|Model
     */
    public function saveNoteHasImage( Note $note, $idImage )
    {

        return $this->getNotesHasImages()::create( [ 'id_note' => $note->id_note, 'id_image' => $idImage ] );
    }

    /**
     * @param $weekStartDate
     * @param $weekEndDate
     * @return mixed
     */
    public function listNotesByWeekQuery( $weekStartDate, $weekEndDate )
    {

        return $this->getModel()::whereBetween( 'notes.created_at', [ $weekStartDate, $weekEndDate ] )
            ->join( 'groups_has_notes as ghn', 'notes.id_note', '=', 'ghn.id_note' )
            ->join( 'groups_has_users as ghu', 'ghn.id_group', '=', 'ghu.id_group' )
            ->whereNull('ghn.deleted_at')
            ->whereNull('ghu.deleted_at')
            ;

    }

    /**
     * @return Note
     */
    public function getModel(): Note
    {

        return $this->model;
    }
}
