<?php

namespace App\Core\Notes\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreNoteRequests
 * @package App\Core\Notes\Requests
 */
class UpdateNoteRequests extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {

        return [
            'name'              => 'required|max:200',
            'description'       => 'required|max:350',
            'id_group'          => 'required|numeric|exists:groups,id_group',
            'images'            => 'sometimes|required',
            'images.*'          => 'required',
            'images.*.image'    => 'sometimes|required|image|max:2048',
            'images.*.id_image' => 'sometimes|required|',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [

        ];
    }
}
