<?php

namespace App\Core\Notes\Services;

use App\Core\Notes\Models\Note;
use App\Core\Notes\Notifications\NoteWeekNotification;
use App\Core\Notes\Repositories\Interfaces\NoteRepositoryInterface;
use App\Core\User\Repositories\interfaces\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class NoteService
 * @package App\Core\Notes\Services
 */
class NoteService
{

    /**
     * @var NoteRepositoryInterface
     */
    private $noteRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * NoteService constructor.
     * @param NoteRepositoryInterface $noteRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct( NoteRepositoryInterface $noteRepository, UserRepositoryInterface $userRepository )
    {

        $this->noteRepository = $noteRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $storeNoteRequests
     * @return mixed
     */
    public function save( $storeNoteRequests )
    {

        $note = $this->noteRepository->getModel();
        $note->fill( $storeNoteRequests->all() );

        return $this->noteRepository->save( $note );

    }

    /**
     * @param Note $note
     * @return Note|Builder|Model|object|null
     */
    public function getNoteRelationWithImages( Note $note )
    {

        return $this->noteRepository->getModel()::with( 'images' )->where( 'id_note', $note->id_note )->first();
    }

    /**
     * @param      $updateNoteRequests
     * @param Note $note
     * @return mixed
     */
    public function update( $updateNoteRequests, Note $note )
    {

        $note->fill( $updateNoteRequests->all() );

        return $this->noteRepository->updateNote( $note );

    }

    /**
     * @param Note       $note
     * @param Collection $images
     */
    public function saveNoteHasImage( Note $note, Collection $images ): void
    {

        $note->images()->createMany( $images->toArray() );

    }

    public function lookingNoteCreateByWeek()
    {

        Carbon::setWeekStartsAt( Carbon::MONDAY );
        Carbon::setWeekEndsAt( Carbon::FRIDAY );

        $now           = Carbon::now();
        $weekStartDate = $now->startOfWeek()->format( 'Y-m-d H:i' );
        $weekEndDate   = $now->endOfWeek()->format( 'Y-m-d H:i' );

        $columns = [ 'notes.*', 'ghu.id_user', ];

        $notes = $this->noteRepository->listNotesByWeekQuery( $weekStartDate, $weekEndDate )
            ->with( 'images' )
            ->get( $columns )
        ;

        $arrayUsers = $notes->unique( 'id_user' )->pluck( 'id_user' )->toArray();
        $users      = $this->userRepository->getAllUserByIdsQuery( $arrayUsers )->get( [ '*' ] );

        if($notes->count()>0){
            $users->map( $this->mapSenNotificationNotesStoreByWeekTransform( $notes ) );
        }
    }

    /**
     * @param $notes
     * @return callable
     */
    private function mapSenNotificationNotesStoreByWeekTransform( $notes ): callable
    {

        return function ( $item, $key ) use ( $notes ) {


            $notesByUserGroup = $notes->where('id_user', $item->id)->unique('id_note');
            $item->notify( new NoteWeekNotification( $notesByUserGroup ) );

            return $item;
        };
    }

}
