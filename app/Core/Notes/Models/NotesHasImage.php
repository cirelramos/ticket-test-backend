<?php
namespace App\Core\Notes\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Core\Notes\Models\NotesHasImage
 *
 * @property int $id_note id note
 * @property timestamp $created_at created at
 * @property timestamp $updated_at updated at
 * @property IdNote $note belongsTo
 * @property int $id_image relation images
 * @property-read \App\Core\Notes\Models\Note $idNote
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage whereIdImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage whereIdNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Core\Notes\Models\Note $notes
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Notes\Models\NotesHasImage whereDeletedAt($value)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Notes\Models\NotesHasImage onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Notes\Models\NotesHasImage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Notes\Models\NotesHasImage withoutTrashed()
 * @property-read \App\Core\Notes\Models\Note $notes
 */
class NotesHasImage extends Model
{

    use SoftDeletes;

    /**
    * Database table name
    */
    protected $table = 'notes_has_images';

    /**
    * Mass assignable columns
    */
    protected $fillable=['id_note'];

    /**
    * Date time columns.
    */
    protected $dates=[];

    /**
    * idNote
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function notes()
    {
        return $this->belongsTo(Note::class,'id_note');
    }




}
