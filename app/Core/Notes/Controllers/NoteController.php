<?php

namespace App\Core\Notes\Controllers;

use App\Core\Groups\Services\GroupService;
use App\Core\Images\Services\ImageService;
use App\Core\Notes\Models\Note;
use App\Core\Notes\Requests\StoreNoteRequests;
use App\Core\Notes\Requests\UpdateNoteRequests;
use App\Core\Notes\Services\NoteService;
use App\Http\Controllers\ApiController;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class NoteController extends ApiController
{

    /**
     * @var NoteService
     */
    private $noteService;

    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @var GroupService
     */
    private $groupService;

    /**
     * NoteController constructor.
     * @param NoteService  $noteService
     * @param ImageService $imageService
     * @param GroupService $groupService
     */
    public function __construct( NoteService $noteService, ImageService $imageService, GroupService $groupService )
    {

        $this->middleware( 'auth:api' );
        $this->noteService  = $noteService;
        $this->imageService = $imageService;
        $this->groupService = $groupService;
    }

    /**
     * @param StoreNoteRequests $storeNoteRequests
     * @return JsonResponse|null
     * @throws Exception
     */
    public function store( StoreNoteRequests $storeNoteRequests ): ?JsonResponse
    {

        $errorMessage   = 'error al crear la nota';
        $successMessage = 'nota creada exitosamente';

        try {

            DB::beginTransaction();

            $note   = $this->noteService->save( $storeNoteRequests );
            $images = $this->imageService->save( $storeNoteRequests );
            $this->noteService->saveNoteHasImage( $note, $images );
            $this->groupService->saveGroupHasNote( $storeNoteRequests, $note );
            DB::commit();

            $responseNote = $this->noteService->getNoteRelationWithImages( $note );

            $this->groupService->sendNotification( $storeNoteRequests, $note );

            return $this->successRespons($responseNote,$successMessage,Response::HTTP_CREATED );
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500 );
        }

    }

    /**
     * @param UpdateNoteRequests $updateNoteRequests
     * @param Note               $note
     * @return JsonResponse|null
     * @throws Exception
     */
    public function update( UpdateNoteRequests $updateNoteRequests, Note $note ): ?JsonResponse
    {

        $errorMessage   = 'error al actualizar la nota';
        $successMessage = 'nota actualizada exitosamente';

        try {

            DB::beginTransaction();

            $note   = $this->noteService->update( $updateNoteRequests, $note );
            $images = $this->imageService->save( $updateNoteRequests );
            $this->imageService->deleteImagesRelationWithNotes( $updateNoteRequests, $note );
            $this->noteService->saveNoteHasImage( $note, $images );
            DB::commit();

            $responseNote = $this->noteService->getNoteRelationWithImages( $note );

            return $this->successRespons($responseNote,$successMessage,Response::HTTP_CREATED );
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500 );
        }
    }

}
