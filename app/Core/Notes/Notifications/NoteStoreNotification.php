<?php

namespace App\Core\Notes\Notifications;

use App\Traits\FileStorageS3;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class NoteStoreNotification
 * @package App\Core\Notes\Notification
 */
class NoteStoreNotification extends Notification implements ShouldQueue
{

    use FileStorageS3;
    use Queueable;

    /**
     * @var
     */
    private $via;

    /**
     * @var int
     */
    private $user;

    private $note;

    /**
     * NoteStoreNotification constructor.
     * @param array $via
     * @param       $note
     */
    public function __construct( array $via, $note )
    {

        $this->user = 0;
        $this->via  = $via;
        $this->note = $note;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via( $notifiable )
    {

        return ( $this->via ) ? $this->via : [ 'mail', 'broadcast' ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail( $notifiable ): MailMessage
    {

        return ( new MailMessage() )->subject( 'Te han creado una nota' )
            ->markdown( 'Notification.Resource.Notes.storeNoteNotificationTemplate', [
                'user' => $notifiable,
                'note' => $this->note,
            ] )
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray( $notifiable ): array
    {

        return [
            'message' => 'han creado una nota para ti',
            'from'    => '',
            'to'      => $notifiable->full_name,
            'image'   => '',
            'link'    => '',
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast( $notifiable ): BroadcastMessage
    {

        $this->user = $notifiable->id;

        return new BroadcastMessage( [
            'data' => [
                'id'                         => $notifiable->id,
                'notifications'              => $notifiable->notifications->count(),
                'notifications_unread'       => $notifiable->unreadNotifications->count(),
                'notifications_bells'        => $notifiable->getBellsNotifications()->count(),
                'notifications_bells_unread' => $notifiable->getBellsNotifications( 'unread' )->count(),
                'note'                       => $this->note,
            ],
        ] );
    }

    /**
     * @return string
     */
    public function broadcastType(): string
    {

        return 'note-notification';
    }

    public function broadcastOn()
    {

        return [ "note-notification-{$this->user}" ];
    }
}

