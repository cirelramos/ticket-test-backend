<?php

namespace App\Core\Auth\Controllers;

use App\Http\Controllers\ApiController;
use App\Traits\Auth\AuthenticatesUsersTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends ApiController
{

    use AuthenticatesUsersTrait;

    /**
     * @var int
     */
    public $maxAttempts = 5; // change to the max attemp you want.

    /**
     * @var int
     */
    public $decayMinutes = 5; // change to the minutes you want

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware( 'auth:api' )->except( [ 'login' ] );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login( Request $request ): ?JsonResponse
    {

        try {
            $this->validateLogin( $request );
            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ( $this->hasTooManyLoginAttempts( $request ) ) {
                $this->fireLockoutEvent( $request );
                $this->sendLockoutResponse( $request );
            }
            if ( $this->attemptLogin( $request ) ) {
                $this->clearLoginAttempts( $request );
                $token = $this->createToken( $request );
                if ( $request->remember ) {
                    $token->token->expires_at = Carbon::now()->minute( 1 );
                }
                $token->token->save();

                return $this->getDataLogin( $token );
            }
            $this->incrementLoginAttempts( $request );
            $this->sendFailedLoginResponse( $request );
        }
        catch ( Exception $exception ) {
            $message = json_decode( $exception->getMessage() ) !== null ? json_decode( $exception->getMessage() )
                : $exception->getMessage();
            $code    = (int) ( $exception->getCode() !== 0 && $exception->getCode() < 512 ? $exception->getCode()
                : 500 );
            $code    = $code < 100 ? 500 : $code;

            return $this->errorResponse($message, 'error login', $code);
        }

    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function createToken( Request $request )
    {

        $user = $request->user();

        return $user->createToken( 'Personal Access Token' );
    }

    /**
     * @param $token
     * @return JsonResponse
     */
    public function getDataLogin( $token ): JsonResponse
    {

        $user = Auth::user();
        $data = [
            'id'                => $user->id,
            'name'              => $user->name,
            'id_type_user'      => $user->id_type_user,
            'email'             => $user->email,
            'access_token'      => $token->accessToken,
            'token_type'        => 'Bearer',
            'user_id_role_real' => $user->id_role,
            'create_at'         => $token->token->created_at,
            'expires_at'        => $token->token->expires_at,
        ];

        return $this->successRespons(  $data, 'usuario logeado' );
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout( Request $request ): JsonResponse
    {

        $request->user()->token()->revoke();

        return $this->successRespons([],'Successfully logged out',Response::HTTP_OK );

    }
}
