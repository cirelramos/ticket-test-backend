<?php

namespace App\Core\Images\Models;

use App\Core\Notes\Models\Note;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * App\Core\Images\Models\Image
 *
 * @property varchar                                              $path       path
 * @property timestamp                                            $created_at created at
 * @property timestamp                                            $updated_at updated at
 * @property Collection             $noteshass  belongsToMany
 * @property int                                                  $id_image
 * @property-read Collection|Note[] $notes
 * @property-read int|null                                        $notes_count
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereIdImage( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Image wherePath( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt( $value )
 * @mixin Eloquent
 * @property string|null                                          $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereDeletedAt( $value )
 * @method static bool|null forceDelete()
 * @method static Builder|Image onlyTrashed()
 * @method static bool|null restore()
 * @method static Builder|Image withTrashed()
 * @method static Builder|Image withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Notes\Models\Note[] $notes
 */
class Image extends Model
{

    use SoftDeletes;

    /**
     * Database table name
     */
    protected $table = 'images';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_image';

    /**
     * Mass assignable columns
     */
    protected $fillable = [ 'path' ];

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * noteshasses
     *
     * @return BelongsToMany
     */
    public function notes()
    {

        return $this->belongsToMany( Note::class, 'notes_has_images' );
    }

}
