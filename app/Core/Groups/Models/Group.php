<?php

namespace App\Core\Groups\Models;

use App\Core\Notes\Models\Note;
use App\Core\User\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Core\Groups\Models\Group
 *
 * @property varchar                                  $name       name
 * @property timestamp                                $created_at created at
 * @property timestamp                                $updated_at updated at
 * @property timestamp                                $deleted_at deleted at
 * @property Collection $shasnote   belongsToMany
 * @property Collection $shasuser   belongsToMany
 * @property int $id_group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\Notes\Models\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Core\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\Group whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\Group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\Group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\Group withoutTrashed()
 */
class Group extends Model
{

    use SoftDeletes;
    /**
     * Database table name
     */
    protected $table = 'groups';

    /**
     * Mass assignable columns
     */
    protected $fillable = [ 'name' ];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_group';

    /**
     * Date time columns.
     */
    protected $dates = [];

    /**
     * shasnotes
     *
     * @return BelongsToMany
     */
    public function notes(): BelongsToMany
    {

        return $this->belongsToMany( Note::class, 'groups_has_notes', 'id_group', 'id_note' );
    }

    /**
     * shasusers
     *
     * @return BelongsToMany
     */
    public function users()
    {

        return $this->belongsToMany( User::class, 'groups_has_users', 'id_group','id_user' )->whereNull('groups_has_users.deleted_at');
    }

}
