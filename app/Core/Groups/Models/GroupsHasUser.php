<?php
namespace App\Core\Groups\Models;

use App\Core\User\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Core\Groups\Models\GroupsHasUser
 *
 * @property int $id_user id user
 * @property timestamp $created_at created at
 * @property timestamp $updated_at updated at
 * @property IdUser $user belongsTo
 * @property int $id_group relation groups
 * @property-read \App\Core\User\Models\User $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser whereIdGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\Groups\Models\GroupsHasUser whereDeletedAt($value)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\GroupsHasUser onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\GroupsHasUser withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Core\Groups\Models\GroupsHasUser withoutTrashed()
 */
class GroupsHasUser extends Model
{

    use SoftDeletes;

    /**
    * Database table name
    */
    protected $table = 'groups_has_users';

    /**
    * Mass assignable columns
    */
    protected $fillable=['id_group','id_user'];

    /**
    * Date time columns.
    */
    protected $dates=[];

    /**
    * idUser
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class,'id_user');
    }




}
