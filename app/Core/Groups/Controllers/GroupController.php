<?php

namespace App\Core\Groups\Controllers;

use App\Core\Groups\Models\Group;
use App\Core\Groups\Requests\AssociateUserGroupRequest;
use App\Core\Groups\Services\GroupService;
use App\Http\Controllers\ApiController;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

/**
 * Class GroupsController
 * @package App\Http\Controllers\Group
 */
class GroupController extends ApiController
{

    /**
     * @var GroupService
     */
    private $groupService;

    /**
     * GroupsController constructor.
     * @param GroupService $groupService
     */
    public function __construct( GroupService $groupService )
    {

        $this->middleware( 'auth:api' );
        $this->groupService = $groupService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {

        $groups = $this->groupService->listGroups();

        return $this->successRespons($groups,'',Response::HTTP_OK );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store( Request $request )
    {
        //
    }

    /**
     * @param AssociateUserGroupRequest $associateUserGroupRequest
     * @param Group                     $group
     * @return JsonResponse
     * @throws Exception
     */
    public function associateUser( AssociateUserGroupRequest $associateUserGroupRequest, Group $group ): JsonResponse
    {

        $errorMessage   = 'error al asociar el usuario';
        $successMessage = 'usuario asociado al grupo exitosamente';

        try {

            DB::beginTransaction();

            $responseGroup = $this->groupService->associateUser( $group, $associateUserGroupRequest );

            DB::commit();

            return $this->successRespons([],$successMessage,Response::HTTP_CREATED );
        }
        catch ( Exception $exception ) {
            DB::rollBack();

            return $this->errorResponse([], $errorMessage, 500 );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return JsonResponse
     */
    public function show( Group $group ): JsonResponse
    {

        $responseGroup = $this->groupService->show( $group );

        return $this->successRespons($responseGroup,'',Response::HTTP_ACCEPTED );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy( $id )
    {
        //
    }
}
