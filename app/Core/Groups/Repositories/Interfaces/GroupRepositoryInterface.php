<?php

namespace App\Core\Groups\Repositories\Interfaces;

use App\Core\Groups\Models\Group;
use App\Core\Groups\Models\GroupsHasUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface GroupRepositoryInterface
 * @package App\Core\Groups\Repositories\Interfaces
 */
interface GroupRepositoryInterface
{

    /**
     * @param $idGroup
     * @return Builder
     */
    public function findOneGroup( $idGroup ): Builder;

    /**
     * @return Group
     */
    public function getModel(): Group;

    /**
     * @return GroupsHasUser
     */
    public function getGroupsHasUser(): GroupsHasUser;

    /**
     * @param       $idUser
     * @param Group $group
     * @return mixed
     */
    public function deleteUserAssociateToGroup( $idUser, Group $group );

    /**
     * @param Group $group
     * @param       $idUser
     * @return GroupsHasUser|Model
     */
    public function saveGroupHasUser( Group $group, $idUser );

}
