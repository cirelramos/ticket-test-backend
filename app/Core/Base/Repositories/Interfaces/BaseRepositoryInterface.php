<?php

namespace App\Core\Base\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface BaseRepositoryInterface
 * @package App\Core\Base\Repositories\Interfaces
 */
interface BaseRepositoryInterface
{

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create( array $attributes );

    /**
     * @param array $attributes
     * @return bool
     */
    public function update( array $attributes ): bool;

    /**
     * @param array  $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all( $columns = [ '*' ], string $orderBy = 'id', string $sortBy = 'asc' );

    /**
     * @param $id
     * @return mixed
     */
    public function find( $id );

    /**
     * @param $id
     * @return mixed
     */
    public function findOneOrFail( $id );

    /**
     * @param array $data
     * @return mixed
     */
    public function findBy( array $data );

    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy( array $data );

    /**
     * @param array $data
     * @return mixed
     */
    public function findOneByOrFail( array $data );

    /**
     * @return bool
     */
    public function delete(): bool;

    /**
     * @param Model       $table
     * @param array       $values
     * @param string|null $index
     * @return bool
     */
    public function updateBulk( Model $table, array $values, string $index = null ): bool;
}
