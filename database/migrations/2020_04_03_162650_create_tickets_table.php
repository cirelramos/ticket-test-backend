<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create( 'tickets', function ( Blueprint $table ) {

            $table->increments( 'id_ticket' );
            $table->integer( 'ticket_requested' )
                ->default( 0 )
                ->comment( '0 is default, 0 dont get ticket, 1 ticket requested ' )
            ;
            $table->integer( 'id_user' )->unsigned()->comment( 'relation users' );
            $table->foreign( 'id_user' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );

            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists( 'tickets' );
    }
}
