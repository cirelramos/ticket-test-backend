<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGroupsHasUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_has_users', function (Blueprint $table) {
            $table->integer('id_group')->unsigned()->comment('relation groups');
            $table->integer('id_user')->unsigned()->comment('relation users');
            $table->foreign('id_group')
                ->references('id_group')
                ->on('groups')
                ->onDelete('cascade')
            ;
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
            ;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_groups_has_users');
    }
}
