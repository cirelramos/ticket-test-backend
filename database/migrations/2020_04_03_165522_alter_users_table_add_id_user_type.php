<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AlterUsersTableAddIdUserType
 */
class AlterUsersTableAddIdUserType extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table( 'users', function ( Blueprint $table ) {

            $table->integer( 'id_type_user' )->nullable()->unsigned()->comment( 'relation users_types' );
            $table->foreign( 'id_type_user' )->references( 'id_type_user' )->on( 'types_users' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table( 'users', function ( Blueprint $table ) {
            $table->dropForeign('users_id_type_user_foreign');
            $table->dropColumn( 'id_type_user' );
        } );

    }
}
