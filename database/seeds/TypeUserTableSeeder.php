<?php

use App\Core\User\Models\TypeUser;
use Illuminate\Database\Seeder;

/**
 * Class GroupsTableSeeder
 */
class TypeUserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {

        DB::statement( 'SET FOREIGN_KEY_CHECKS=0;' );
        TypeUser::truncate();
        TypeUser::flushEventListeners();

        $typesUsersArray   = [];
        $typesUsersArray[] = [ 'name' => 'administrador' ];
        $typesUsersArray[] = [ 'name' => 'usuario' ];
        TypeUser::insert( $typesUsersArray );
        DB::statement( 'SET FOREIGN_KEY_CHECKS=1;' );
    }
}
